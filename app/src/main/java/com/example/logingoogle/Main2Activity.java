package com.example.logingoogle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {

    GoogleSignInClient mGoogleSignInClient;
    Button sign_out;
    TextView nameTV;
    TextView emailTV;
    //TextView idTV;

    ImageView photoIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        final ArrayList<String> listasRep;
        final ListView lista = (ListView)findViewById(R.id.lista);
        //final String[] listasRep = {"", "", "", ""};

        listasRep = new ArrayList<String>();
        listasRep.add("Lista Vacía");
        listasRep.add("Música para dormir");
        listasRep.add("Pop en español");
        listasRep.add("Canciones favoritas");
        listasRep.add("Música Romántica");


        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listasRep);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                //Toast.makeText(MainActivity.this, "Has pulsado: "+ listasRep.get(position), Toast.LENGTH_LONG).show();
                //Intent intent = new Intent(MainActivity.this, ListaCancionesRomanticas.class);

                //intent.putExtra("objetoData" , listasRep.get(position));
                //listasRep.get(position);
                switch(position){

                    case 4:
                        Intent intent = new Intent(Main2Activity.this, CancionesRomanticas.class);
                        startActivity(intent);
                        break;
                    case 0:
                        Toast.makeText(Main2Activity.this, "Has pulsado Lista Vacía", Toast.LENGTH_LONG).show();
                        break;

                    case 1:
                        Toast.makeText(Main2Activity.this, "Has pulsado Música para dormir ", Toast.LENGTH_LONG).show();
                        break;

                    case 2:
                        Toast.makeText(Main2Activity.this, "Has pulsado pop en español", Toast.LENGTH_LONG).show();
                        break;

                    case 3:
                        //Intent intent2 = new Intent(Main2Activity.this, CancionesFavoritas.class);
                        //startActivity(intent2);
                        Toast.makeText(Main2Activity.this, "Has pulsado Canciones favoritas", Toast.LENGTH_LONG).show();
                        break;
                }


            }
        });




        sign_out = findViewById(R.id.salir);
        nameTV = findViewById(R.id.nombre);
        emailTV = findViewById(R.id.email);
        //idTV = findViewById(R.id.id);
        photoIV = findViewById(R.id.photo);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(Main2Activity.this);
        if (acct != null) {
            String personName = acct.getDisplayName();
            String personGivenName = acct.getGivenName();
            String personFamilyName = acct.getFamilyName();
            String personEmail = acct.getEmail();
            //String personId = acct.getId();
            Uri personPhoto = acct.getPhotoUrl();

            nameTV.setText("Nombre de Usuario: "+personName);
            emailTV.setText("Correo Electrónico: "+personEmail);
            //idTV.setText("ID: "+personId);
            Glide.with(this).load(personPhoto).into(photoIV);
        }

        sign_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signOut();
            }
        });
    }

    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(Main2Activity.this,"Successfully signed out", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(Main2Activity.this, MainActivity.class));
                        finish();
                    }
                });
    }
}
